package org.august.uid;

import org.august.uid.transport.NettyServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UidGeneratorServerApplication implements ApplicationRunner {

	@Autowired
	private NettyServer nettyServer;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		nettyServer.start();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(UidGeneratorServerApplication.class, args);
	}

}

