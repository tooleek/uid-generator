package org.august.uid.service;

import org.august.uid.msg.PackageMsg;

public interface MsgHandlerService {
	
	public void handle(PackageMsg packageMsg);

}
