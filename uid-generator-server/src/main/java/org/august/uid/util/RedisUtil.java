package org.august.uid.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

@Component
public class RedisUtil {
	
	@Autowired
	RedisConnectionFactory redisConnectionFactory;
	
	/**
	 * 生成原子自增id
	 * @param key
	 * @return
	 */
	public long getAtoimcId(String key) {
		RedisAtomicLong atomicId = new RedisAtomicLong(key, redisConnectionFactory);
		long id = atomicId.getAndIncrement();
		return id;
	}

}
