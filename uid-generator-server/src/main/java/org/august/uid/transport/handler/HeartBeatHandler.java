package org.august.uid.transport.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelHandler.Sharable;

@Component
@Sharable
public class HeartBeatHandler extends ChannelInboundHandlerAdapter {
	
	private static final Logger logger = LoggerFactory.getLogger(HeartBeatHandler.class);
	
    @Override  
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    	
        System.out.println(ctx.channel().remoteAddress() + "->Server :" + msg.toString());  
    }  
  
    @Override  
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {  
    	logger.error(cause.getMessage(),cause);
		ctx.close();
    }  
  
}
