package org.august.uid.generator;

public abstract class StringUidGenerator implements UidGenerator<String> {

	@Override
	public abstract String generate();

}
