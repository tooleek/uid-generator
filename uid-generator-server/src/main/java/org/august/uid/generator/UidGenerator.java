package org.august.uid.generator;

public interface UidGenerator<T> {
	
	/**
	 * 生成uid
	 * @return
	 */
	public T generate();

}
