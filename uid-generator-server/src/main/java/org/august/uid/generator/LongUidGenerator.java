package org.august.uid.generator;

public abstract class LongUidGenerator implements UidGenerator<Long> {

	@Override
	public abstract Long generate();
	
}