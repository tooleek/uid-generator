package org.august.uid.generator;

import org.august.uid.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RedisLongUidGenerator extends LongUidGenerator {

	@Autowired
	private RedisUtil redisUtil;
	
	@Override
	public Long generate() {
		return redisUtil.getAtoimcId("ss");
	}

}
