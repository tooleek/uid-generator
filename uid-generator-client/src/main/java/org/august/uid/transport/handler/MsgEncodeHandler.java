package org.august.uid.transport.handler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.august.uid.msg.PackageMsg;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class MsgEncodeHandler extends MessageToByteEncoder<PackageMsg> {

	@Override
	protected void encode(ChannelHandlerContext ctx, PackageMsg msg, ByteBuf out) throws Exception {
		byte[] datas = objectToByte(msg);
        out.writeBytes(datas);
        ctx.flush();
	}

	private byte[] objectToByte(Object obj) {
		byte[] bytes = null;
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        ObjectOutputStream oo = null;
        try {
            oo = new ObjectOutputStream(bo);
            oo.writeObject(obj);
            bytes = bo.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                oo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return (bytes);
	}

}
