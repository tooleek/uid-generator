package org.august.uid.msg;

import java.io.Serializable;



public class PackageMsg implements Serializable {
	
	private static final long serialVersionUID = 2354580604438599822L;
	
	private String packageType="heart";

	public String getPackageType() {
		return packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}
	
	
	
}
