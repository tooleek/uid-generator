package org.august.uid.msg;

import org.august.uid.enumeration.GeneratorEnum;

public abstract class GeneratorPackageMsg extends PackageMsg {

	private GeneratorEnum generator;
	
	public abstract GeneratorEnum getGenerator();
	
}
