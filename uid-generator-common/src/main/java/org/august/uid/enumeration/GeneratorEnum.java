package org.august.uid.enumeration;

public enum GeneratorEnum {
	/**
	 * long型的id
	 */
	LONG,
	/**
	 * 字符串型的id
	 */
	STRING
}
