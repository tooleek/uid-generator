package org.august.uid.common;

public class Constants {

	public static final int DEFAULT_IO_THREADS = Math.min(Runtime.getRuntime().availableProcessors() + 1, 32);
	
}
