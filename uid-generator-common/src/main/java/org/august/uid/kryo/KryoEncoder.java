package org.august.uid.kryo;

import org.august.uid.msg.PackageMsg;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class KryoEncoder extends MessageToByteEncoder<PackageMsg> {

	@Override
	protected void encode(ChannelHandlerContext ctx, PackageMsg msg, ByteBuf out) throws Exception {
		byte[] bytes = KryoSerializer.serialize(msg);
		out.writeBytes(bytes);
		ctx.flush();
	}
	
}
