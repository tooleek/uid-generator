package org.august.uid.kryo;

import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;

public class ThreadLocalKryoFactory {

	private static final ThreadLocal<Kryo> kryoLocal=new ThreadLocal<Kryo>() {
		@Override
		protected Kryo initialValue() {
			Kryo kryo = new Kryo();
	        kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(
	                    new StdInstantiatorStrategy()));
	        return kryo;
		}
	};
	
	public static Kryo getKryoInstance() {
		return kryoLocal.get();
	}
	
}
