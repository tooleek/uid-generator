package org.august.uid.kryo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;


public class KryoSerializer {
	
	public static byte[] serialize(Object object) throws IOException {
		Kryo kryo=ThreadLocalKryoFactory.getKryoInstance();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Output output = new Output(baos);
        kryo.writeClassAndObject(output, object);
        output.flush();
        output.close();

        byte[] b = baos.toByteArray();
        
        baos.flush();
        baos.close();
        
        return b;
        //out.writeBytes(b);
	}
	
	public static Object deserialize(InputStream inputStream) {
        Input input = new Input(inputStream);
        Kryo kryo = ThreadLocalKryoFactory.getKryoInstance();
        return kryo.readClassAndObject(input);
	}

}
